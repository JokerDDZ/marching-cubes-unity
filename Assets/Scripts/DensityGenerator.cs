using UnityEngine;
using System.Collections.Generic;

public class DensityGenerator : MonoBehaviour
{
    private const int _threadSize = 8;
    [SerializeField] protected ComputeShader _densityComputeShader;
    protected List<ComputeBuffer> _bufferRel;

    public virtual ComputeBuffer Generate(ComputeBuffer pointsBuffer, int numPointsPerAxis, float boundsSize, Vector3 worldBounds, Vector3 centre, Vector3 offset, float spacing)
    {
        int numPoints = numPointsPerAxis * numPointsPerAxis * numPointsPerAxis;
        int numThreadsPerAxis = Mathf.CeilToInt(numPointsPerAxis / (float)_threadSize);
        // Points buffer is populated inside shader with pos (xyz) + density (w).
        // Set paramaters
        _densityComputeShader.SetBuffer(0, "points", pointsBuffer);
        _densityComputeShader.SetInt("numPointsPerAxis", numPointsPerAxis);
        _densityComputeShader.SetFloat("boundsSize", boundsSize);
        _densityComputeShader.SetVector("centre", new Vector4(centre.x, centre.y, centre.z));
        _densityComputeShader.SetVector("offset", new Vector4(offset.x, offset.y, offset.z));
        _densityComputeShader.SetFloat("spacing", spacing);
        _densityComputeShader.SetVector("worldSize", worldBounds);

        // Dispatch shader
        _densityComputeShader.Dispatch(0, numThreadsPerAxis, numThreadsPerAxis, numThreadsPerAxis);

        if (_bufferRel != null)
        {
            foreach (var b in _bufferRel)
            {
                b.Release();
            }
        }

        return pointsBuffer;
    }
}
