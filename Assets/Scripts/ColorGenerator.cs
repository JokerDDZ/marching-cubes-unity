using UnityEngine;

public class ColorGenerator : MonoBehaviour
{
    public Material GetMat { get => _mat; }
    [SerializeField] private Material _mat;
    private MeshGenerator _meshGenerator;
    private Texture2D _texture;
    private int _textureRes = 100;
    public Color[] GetColors { get => _colors; }
    private Color[] _colors = new Color[5];

    private void Awake()
    {
        _meshGenerator = GetComponent<MeshGenerator>();
    }

    public void SetupColorForMaterial()
    {
        if (_texture == null || _texture.width != _textureRes)
        {
            _texture = new Texture2D(_textureRes, 1, TextureFormat.RGBA32, false);
        }

        Color[] colors = new Color[_texture.width];
        Gradient gradient = new Gradient();
        GradientColorKey[] colorKey = new GradientColorKey[5];
        GradientAlphaKey [] alphas = new GradientAlphaKey[5];

        for (int i = 0; i < _colors.Length; i++)
        {
            float inter = 1f / (float)_colors.Length * (float)i;
            colorKey[i].color = _colors[i];
            colorKey[i].time = inter;
            alphas[i].alpha = 1f;
            alphas[i].time = inter;
        }

        gradient.SetKeys(colorKey,alphas);

        for (int i = 0; i < _textureRes; i++)
        {
            Color gradientCol = gradient.Evaluate(i / (_textureRes - 1f));
            colors[i] = gradientCol;
        }

        _texture.SetPixels(colors);
        _texture.Apply();

        float boundsY = _meshGenerator.GetBoundSize;

        _mat.SetFloat("boundsY", boundsY);
        _mat.SetTexture("ramp", _texture);
    }
}
