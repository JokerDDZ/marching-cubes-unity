using System.Collections.Generic;
using UnityEngine;

public class NoiseDensity : DensityGenerator
{
    public int SetSeed{set => _seed = value;}
    [SerializeField] private int _seed;
    private int _numOctaves = 6;
    private float _lacunarity = 2f;
    [SerializeField] private float _persistence = .5f;
    [SerializeField] private float _noiseScale = 1f;
    [SerializeField] private float _noiseWeight = 1f;
    [SerializeField] private bool _closeEdges = false;
    [SerializeField] private float _floorOffset = 1f;
    [SerializeField] private float _weightMulti = 1f;
    [SerializeField] private float _hardFloorHeight = 2f;
    [SerializeField] private float _hardFloorWeight = 2f;

    private Vector4 _shaderParams = new Vector4(1f,0f,0f,0f);

    public float SetNoiseScale{set => _noiseScale = value;}

    public override ComputeBuffer Generate(ComputeBuffer pointsBuffer, int numPointsPerAxis, float boundsSize, Vector3 worldBounds, Vector3 centre, Vector3 offset, float spacing)
    {
        _bufferRel = new List<ComputeBuffer>();

        // Noise parameters
        var prng = new System.Random(_seed);
        var offsets = new Vector3[_numOctaves];
        float offsetRange = 1000;
        for (int i = 0; i < _numOctaves; i++)
        {
            offsets[i] = new Vector3((float)prng.NextDouble() * 2 - 1, (float)prng.NextDouble() * 2 - 1, (float)prng.NextDouble() * 2 - 1) * offsetRange;
        }

        var offsetsBuffer = new ComputeBuffer(offsets.Length, sizeof(float) * 3);
        offsetsBuffer.SetData(offsets);
        _bufferRel.Add(offsetsBuffer);

        _densityComputeShader.SetVector("centre", new Vector4(centre.x, centre.y, centre.z));
        _densityComputeShader.SetInt("octaves", Mathf.Max(1, _numOctaves));
        _densityComputeShader.SetFloat("lacunarity", _lacunarity);
        _densityComputeShader.SetFloat("persistence", _persistence);
        _densityComputeShader.SetFloat("noiseScale", _noiseScale);
        _densityComputeShader.SetFloat("noiseWeight", _noiseWeight);
        _densityComputeShader.SetBool("closeEdges", _closeEdges);
        _densityComputeShader.SetBuffer(0, "offsets", offsetsBuffer);
        _densityComputeShader.SetFloat("floorOffset", _floorOffset);
        _densityComputeShader.SetFloat("weightMultiplier", _weightMulti);
        _densityComputeShader.SetFloat("hardFloor", _hardFloorHeight);
        _densityComputeShader.SetFloat("hardFloorWeight", _hardFloorWeight);

        _densityComputeShader.SetVector("params", _shaderParams);

        return base.Generate(pointsBuffer, numPointsPerAxis, boundsSize, worldBounds, centre, offset, spacing);
    }
}
