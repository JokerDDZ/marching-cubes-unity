using Tayx.Graphy;
using UnityEngine;
using UnityEngine.Profiling;

public class MeshEditor : MonoBehaviour
{
    [SerializeField] private GameObject _centerGO;
    [SerializeField] private ComputeShader _editingCompute;
    [SerializeField] private GraphyManager _fpsCounter;
    [SerializeField] private Transform _camera;
    [SerializeField] private GUIController _guiController;
    private Tile _closestOne;
    private MeshGenerator _meshGenerator;
    private int[] _modifRead;
    private float _radius = 3f;
    public float SetRadius { set => _radius = value; }
    private float _speed = 1f;
    public float SetSpeed { set => _speed = value; }
    private const int _threadSize = 8;
    public bool SetShowCenter { set => _showCenter = value; }
    private bool _showCenter = false;

    //For String optimization
    private const string _nameOfRadius = "radius";
    private const string _nameOfDeltaTime = "deltaTime";
    private const string _nameOfSpeed = "speed";
    private const string _nameOfRPerAxis = "numPointsPerAxis";
    private const string _nameOfCenter = "center";
    private const string _nameOfPoints = "points";
    private const string _nameOfAdding = "adding";
    //Profiler
    private const string _nameOnlyParticlesCalcProfiler = "Editing mesh - only particles";
    private const string _nameOtherProfiler = "Other";
    private const string _nameInsideProfilerOne = "InsideOne";
    private const string _nameInsideProfilerTwo = "InsideTwo";
    private const string _nameInsideProfilerThree = "InsideThree";
    private const string _nameInsideProfilerFourth = "InsideFourth";


    private void Awake()
    {
        _meshGenerator = GetComponent<MeshGenerator>();
        _modifRead = new int[1];
    }

    private void Update()
    {
        if (_fpsCounter.AudioModuleState == GraphyManager.ModuleState.FULL) return;

        Profiler.BeginSample(_nameOtherProfiler);
        Profiler.BeginSample(_nameInsideProfilerOne);
        float minDist = Mathf.Infinity;

        foreach (Tile t in _meshGenerator.Tiles)
        {
            float dist = Vector3.Distance(_camera.position, _meshGenerator.CentreFromCoord(t.GetCoord));

            if (dist < minDist)
            {
                minDist = dist;
                _closestOne = t;
            }

            t.SetCloseOne = false;
        }

        _closestOne.SetCloseOne = true;

        Profiler.EndSample();
        Profiler.BeginSample(_nameInsideProfilerTwo);
        int numberOfTilesUpdated = 0;
        Vector3 center = _meshGenerator.CentreFromCoord(_closestOne.GetCoord);
        _centerGO.SetActive(_showCenter);
        _centerGO.transform.position = center;
        Profiler.BeginSample(_nameInsideProfilerThree);
        if (_closestOne.GetModified)
        {
            _meshGenerator.UpdateTilesMesh(_closestOne, false);
            numberOfTilesUpdated++;
        }
        Profiler.EndSample();
        Profiler.BeginSample(_nameInsideProfilerFourth);
        foreach (Tile t in _closestOne.GetNeighbours)
        {
            if (t == _closestOne) continue;

            Vector3 pos = _meshGenerator.CentreFromCoord(t.GetCoord);
            Vector3 dirFromMainTile = (pos - center).normalized;
            float dot = Vector3.Dot(dirFromMainTile, _camera.forward);

            if (dot > -.3f)
            {
                //Debug.Log($"Dot: {dot}");
                //Debug.Log($"Dir1{dirFromCamera},Dir2{dirFromMainTile}");
                if (t.GetModified)
                {
                    _meshGenerator.UpdateTilesMesh(t, false);
                    numberOfTilesUpdated++;
                }


                if (_showCenter) Debug.DrawLine(center, center + dirFromMainTile * 2f, Color.green);
            }
            else
            {
                if (_showCenter) Debug.DrawLine(center, center + dirFromMainTile * 2f, Color.red);
            }
        }
        Profiler.EndSample();
        Profiler.EndSample();
        Profiler.EndSample();

        _guiController.SetNumberOfUpdated = $"Updated tiles: {numberOfTilesUpdated}";
        //Debug.Log($"Number of tiles updated: {numberOfTilesUpdated}");

        if (Input.GetMouseButton(0))
        {
            _editingCompute.SetBool(_nameOfAdding, true);
        }

        if (Input.GetMouseButton(1))
        {
            _editingCompute.SetBool(_nameOfAdding, false);
        }

        if (Input.GetMouseButton(0) || Input.GetMouseButton(1))
        {
            Vector3 cameraPos = _camera.transform.position;
            Vector3 posToCheck = _camera.transform.position + _camera.transform.forward * (_radius + .3f);
            int numVoxelsPerAxis = _meshGenerator.GetPointPerTile - 1;
            int numThreadsPerAxis = Mathf.CeilToInt(numVoxelsPerAxis / (float)_threadSize);

            _editingCompute.SetFloat(_nameOfRadius, _radius);
            _editingCompute.SetFloat(_nameOfDeltaTime, Time.deltaTime);
            _editingCompute.SetFloat(_nameOfSpeed, _speed);
            _editingCompute.SetInt(_nameOfRPerAxis, _meshGenerator.GetPointPerTile);
            _editingCompute.SetVector(_nameOfCenter, new Vector4(posToCheck.x, posToCheck.y, posToCheck.z, 0f));

            Profiler.BeginSample(_nameOnlyParticlesCalcProfiler);

            foreach (Tile t in _closestOne.GetNeighbours)
            {
                _editingCompute.SetBuffer(0, _nameOfPoints, t.GetVertexBuffer);
                _editingCompute.Dispatch(0, numThreadsPerAxis, numThreadsPerAxis, numThreadsPerAxis);

                Vector3 coordToWorldSpace = _meshGenerator.CentreFromCoord(t.GetCoord);
                Vector3 min = coordToWorldSpace - new Vector3(_meshGenerator.GetBoundSize/2f,_meshGenerator.GetBoundSize/2f,_meshGenerator.GetBoundSize/2f);
                Vector3 max = coordToWorldSpace + new Vector3(_meshGenerator.GetBoundSize/2f,_meshGenerator.GetBoundSize/2f,_meshGenerator.GetBoundSize/2f);

                if(Utils.DoesCubeIntersectSphere(min,max,posToCheck,_radius))
                {
                    t.SetModified = true;
                }
            }

            Profiler.EndSample();
        }
    }
}
