using UnityEngine;
using System.Collections.Generic;

public class Tile : MonoBehaviour
{
    public ComputeShader SetForReset { set => _forReset = value; }
    private ComputeShader _forReset;
    public ParticleDebug SetParticleDebug { set => _particleDebug = value; }
    public ParticleDebug GetParticleDebug { get => _particleDebug; }
    private ParticleDebug _particleDebug;
    public Mesh Mesh;
    private MeshFilter _meshFilter;
    private MeshRenderer _meshRenderer;
    private MeshCollider _meshCollider;
    public ComputeBuffer GetVertexBuffer { get => _vertexBuffer; }
    private ComputeBuffer _vertexBuffer;
    public List<Tile> GetNeighbours { get => _neighbours; }
    private List<Tile> _neighbours = new List<Tile>();
    public Vector3Int SetCoord { set => _coord = value; }
    public Vector3Int GetCoord { get => _coord; }
    private Vector3Int _coord;
    public bool SetModified { set => _modified = value; }
    public bool GetModified { get => _modified; }
    private bool _modified = false;
    public bool SetCloseOne { set => _closeOne = value; }
    public bool GetCloseOne { get => _closeOne; }
    private bool _closeOne = false;

    private void Awake()
    {
        _meshFilter = gameObject.AddComponent<MeshFilter>();
        _meshRenderer = gameObject.AddComponent<MeshRenderer>();
        _meshCollider = gameObject.AddComponent<MeshCollider>();

        Mesh = new Mesh();
        Mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
        _meshFilter.sharedMesh = Mesh;
        _meshRenderer.material = FindObjectOfType<ColorGenerator>().GetMat;
    }

    public void SetVertexBufferWithArray(float[] data, int resolution)
    {
        ComputeBuffer cb = new ComputeBuffer(resolution, sizeof(float) * 4);

        if (data != null)
        {
            cb.SetData(data);
        }

        _vertexBuffer = cb;
    }

    public void SetUp()
    {
        Mesh = _meshFilter.sharedMesh;
        _modified = false;
    }
}
