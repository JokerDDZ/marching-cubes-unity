Shader "Custom/Particle" {
	Properties     
    {         
    }  

	SubShader {
 
		CGPROGRAM

		sampler2D _MainTex;
 
		struct Input {
			float2 uv_MainTex;
			float2 uv_BumpMap;
			float3 worldPos;
		};

		fixed4 _Color;
 
        #pragma surface surf NoLighting vertex:vert nolightmap
        #pragma instancing_options procedural:setup

        float3 _BoidPosition;
        float3 _coord;
		float _colForSurf;
		float a;
		float b;

        #ifdef UNITY_PROCEDURAL_INSTANCING_ENABLED
        struct Particle
        {
            float4 position;
        };

        StructuredBuffer<Particle> particleBuffer; 
        #endif

		fixed4 LightingNoLighting(SurfaceOutput s, fixed3 lightDir, fixed atten)
     	{
         fixed4 c;
         c.rgb = s.Albedo; 
         c.a = s.Alpha;
         return c;
     	}


        void vert(inout appdata_full v, out Input data)
        {
            UNITY_INITIALIZE_OUTPUT(Input, data);

            #ifdef UNITY_PROCEDURAL_INSTANCING_ENABLED
                v.vertex.xyz += _BoidPosition;
            #endif
        }

        void setup()
        {
            #ifdef UNITY_PROCEDURAL_INSTANCING_ENABLED
                _BoidPosition = particleBuffer[unity_InstanceID].position;
				_colForSurf = particleBuffer[unity_InstanceID].position.w;
            #endif
        }
 
        void surf (Input IN, inout SurfaceOutput o) {
			float weight = smoothstep(0.2,0.8,a*_colForSurf + b);
            fixed4 c = weight;
			o.Albedo = c.rgb;
    	}
 
        ENDCG
   }
}