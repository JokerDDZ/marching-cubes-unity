Shader "Custom/ParticleVertFrag" {
	Properties     
    {         
    }  

	SubShader {
        Pass{
            Tags {"Queue"="Geometry" "IgnoreProjector"="True" "RenderType"="Opaque"}
			Lighting Off
			Fog { Mode Off }

            Blend SrcAlpha OneMinusSrcAlpha 

            CGPROGRAM

           // #pragma instancing_options procedural:setup
		    #pragma vertex vert
		    #pragma fragment frag
		    #pragma multi_compile_fwdbase nolightmap nodirlightmap nodynlightmap novertexlight
            #pragma multi_compile_instancing
		    #pragma target 4.5

		    #include "UnityCG.cginc"
		    #include "UnityLightingCommon.cginc"
		    #include "AutoLight.cginc"

		    float _colForSurf;
		    float a;
		    float b;

            #if SHADER_TARGET >= 45
            struct Particle
            {
                float4 position;
            };

			StructuredBuffer<Particle> particleBuffer;
		    #endif

            struct v2f
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
                uint idHelper:SV_InstanceID;
                UNITY_VERTEX_INPUT_INSTANCE_ID 
			};

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                uint idHelper : SV_InstanceID;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            v2f vert (appdata v, uint instanceID : SV_InstanceID)
			{
                v2f o;
                UNITY_SETUP_INSTANCE_ID(v);

                #if SHADER_TARGET >= 45
                    float4 data = particleBuffer[instanceID].position;
                #else
                    float4 data = 0;
                #endif

                float3 worldPosition = data.xyz + v.vertex.xyz;   
                o.pos = mul(UNITY_MATRIX_VP, float4(worldPosition, 1.0f));
                o.uv = v.uv;
                o.idHelper = v.idHelper;
                return o;
			}

            fixed4 frag (v2f i) : SV_Target
			{
                UNITY_SETUP_INSTANCE_ID(i);
                float val = particleBuffer[i.idHelper].position.w;
                float weight = smoothstep(0.2,0.8,a*val + b);
				fixed4 col = float4(weight,weight,weight,1);
				return col;
			}

			ENDCG
        }
   }
}