using UnityEngine;

public class Timelapse : MonoBehaviour
{
    [SerializeField] private Light _light;
    public Color SetLightColor { set => _lightColor = value; }
    private Color _lightColor = Color.white;
    public float SetTimeLapseSpeed { set => _timeLapseSpeed = value; }
    private float _timeLapseSpeed = 1f;
    public bool SetTimeLapse { set => _timeLapse = value; }
    private bool _timeLapse = false;

    private void Update()
    {
        if (_timeLapse)
        {
            _light.transform.Rotate(Vector3.right * _timeLapseSpeed * Time.deltaTime, Space.World);
        }

        _light.color = _lightColor;
    }
}
