using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class SaveFilesManager : MonoBehaviour
{
    public static SaveFilesManager Instance;
    [SerializeField] private MeshGenerator _meshGenerator;
    [SerializeField] private GUIBinder _guiBinder;
    public string GetPath { get => _path; }
    private string _path;

    private void Awake()
    {
        Instance = this;
        _path = Application.persistentDataPath + "/MarchingCubes";
        Directory.CreateDirectory(_path);
    }

    public string SaveFile()
    {
        DateTime now = DateTime.Now;

        string fileName = null;
        fileName += now.Day + "_";
        fileName += now.Month + "_";
        fileName += now.Year + "_";
        fileName += now.Hour + "_";
        fileName += now.Minute + "_";
        fileName += now.Second;
        fileName += ".json";

        string savePath = _path + $"/{fileName}";

        SaveData sd = _guiBinder.GetValuesForSave();
        string dataAsJson = JsonUtility.ToJson(sd);
        using StreamWriter writer = new StreamWriter(savePath);
        writer.Write(dataAsJson);

        return fileName;
    }

    public void LoadFile(string fileName)
    {
        string pathToFile = _path + $"/{fileName}";
        using StreamReader reader = new StreamReader(pathToFile);
        string json = reader.ReadToEnd();
        SaveData data = JsonUtility.FromJson<SaveData>(json);
        _guiBinder.SetValuesFromLoad(data);

        StartCoroutine(LoadFileDelayedSetup(data));
    }

    private IEnumerator LoadFileDelayedSetup(SaveData data)
    {
        yield return 0;

        _meshGenerator.RefreshTerrain();

        for (int x = 0; x < _meshGenerator.GetNumberOfTiles.x; x++)
        {
            for (int y = 0; y < _meshGenerator.GetNumberOfTiles.y; y++)
            {
                for (int z = 0; z < _meshGenerator.GetNumberOfTiles.z; z++)
                {
                    Tile tile = _meshGenerator.GetTilesAsMatrix[x, y, z];

                    Vector3Int coordinates = new Vector3Int(x, y, z);
                    SaveData.CoordinatesValues handler = data.PointsAndTheirValues.Find(x => x.Coordinates == coordinates);
                    tile.SetVertexBufferWithArray(handler.Values, data.NumPoints * data.NumPoints * data.NumPoints);
                }
            }
        }

        yield return 0;

        _meshGenerator.RefreshTerrain(false);
    }

    public void DeleteFile(string name)
    {
        string savePath = _path + $"/{name}";
        Debug.Log("Delete save file");
        File.Delete(savePath);
    }

    public class SaveData
    {
        public Vector3Int NumberOfTiles;
        public List<CoordinatesValues> PointsAndTheirValues = new List<CoordinatesValues>();

        public float BoundSize;
        public float IsoLevel;
        public float NoiseScale;
        public float SizeOfNoiseDebug;
        public float Radius;
        public float ChangeTerrainSpeed;
        public float TimeLapseSpeed;

        public int NumPoints;
        public int Seed;

        public bool ShowCenter;
        public bool NoiseDebug;
        public bool DrawGizmos;
        public bool AutoUpdate;
        public bool Timelapse;

        //terrain colors
        public Color[] TerrainColors;
        public Color LightColor;

        public SaveData(Vector3Int numberOfTiles, MeshGenerator meshGenerator, int numberOfPointsPerTile)
        {
            NumberOfTiles = numberOfTiles;
            TerrainColors = new Color[5];
            NumPoints = numberOfPointsPerTile;

            for (int x = 0; x < numberOfTiles.x; x++)
            {
                for (int y = 0; y < numberOfTiles.y; y++)
                {
                    for (int z = 0; z < numberOfTiles.z; z++)
                    {
                        Vector3Int coordinates = new Vector3Int(x, y, z);
                        Tile tile = meshGenerator.GetTilesAsMatrix[x, y, z];
                        float[] arr = new float[4 * NumPoints * NumPoints * NumPoints];
                        tile.GetVertexBuffer.GetData(arr);

                        // foreach (float data in arr)
                        // {
                        //     Debug.Log($"Data {data}");
                        // }

                        CoordinatesValues cv = new CoordinatesValues(coordinates, arr);
                        PointsAndTheirValues.Add(cv);
                    }
                }
            }
        }

        [Serializable]
        public struct CoordinatesValues
        {
            public CoordinatesValues(Vector3 coord, float[] values)
            {
                Coordinates = coord;
                Values = values;
            }

            public Vector3 Coordinates;
            public float[] Values;
        }
    }
}
