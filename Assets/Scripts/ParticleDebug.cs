using UnityEngine;

public class ParticleDebug : MonoBehaviour
{
    [SerializeField] private Mesh _mesh;
    public Material material;
    private ComputeBuffer argsBuffer;

    private Vector3[] _vertsPattern;
    uint[] args = new uint[5] { 0, 0, 0, 0, 0 };
    private Bounds bounds;
    private bool _initialized = false;
    public bool SetDebugEnabled { set => _debugEnabled = value; }
    private bool _debugEnabled = false;
    public float SetCurrSizeNoise { set => _currSizeNoise = value; }
    private float _currSizeNoise = 1f;
    private float _oldSizeNoise = 1f;

    void Start()
    {
        _vertsPattern = new Vector3[_mesh.vertices.Length];

        for (int i = 0; i < _vertsPattern.Length; i++)
        {
            _vertsPattern[i] = _mesh.vertices[i];
        }

        _mesh = Instantiate(_mesh);
        bounds = new Bounds(Vector3.zero, Vector3.one * 1000);
        material = new Material(material);
    }

    void Update()
    {
        if (!_initialized || !_debugEnabled) return;

        if (_currSizeNoise != _oldSizeNoise)
        {
            _oldSizeNoise = _currSizeNoise;
            Utils.ChangeMeshScale(_mesh, _currSizeNoise, _vertsPattern);
        }

        Graphics.DrawMeshInstancedIndirect(_mesh, 0, material, bounds, argsBuffer);
    }

    public void RefreshDebug(ComputeBuffer cb)
    {
        _initialized = true;
        int particleCount = cb.count;
        Vector2 line = Utils.FormulaLinearEquationForDebug(-8f, 8f);

        if (argsBuffer != null)
        {
            argsBuffer.Release();
        }

        argsBuffer = new ComputeBuffer(1, 5 * sizeof(uint), ComputeBufferType.IndirectArguments);
        args[0] = (uint)_mesh.GetIndexCount(0);
        args[1] = (uint)particleCount;
        args[2] = (uint)_mesh.GetIndexStart(0);
        args[3] = (uint)_mesh.GetBaseVertex(0);
        args[4] = 0;
        argsBuffer.SetData(args);
        material.SetFloat("a", line.x);
        material.SetFloat("b", line.y);
        //Debug.Log($"Get from material: {material.GetFloat("a")}");
        material.SetBuffer("particleBuffer", cb);
    }

    void OnDestroy()
    {
        if (argsBuffer != null) argsBuffer.Release();
    }
}
