using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class GUIElementController : MonoBehaviour
{
    public string SetHeader { set => _header.text = value; }
    [SerializeField] private Text _header;
    [Header("Update objects")]
    [SerializeField] private Toggle _boolToggle;
    private Color _colorReturnValue = Color.white;
    private bool _boolReturnValue = false;
    [Header("Float")]
    #region float
    [SerializeField] private Slider _floatSlider;
    [SerializeField] private Text _floatText;
    [SerializeField] private Text _minText;
    [SerializeField] private Text _maxText;
    private float _floatReturnValue = 0f;
    private float _min = 0f;
    private float _max = 100f;
    #endregion

    [Header("Int")]
    #region int
    [SerializeField] private Slider _intSlider;
    [SerializeField] private Text _intText;
    [SerializeField] private Text _minIntText;
    [SerializeField] private Text _maxIntText;

    private int _intReturnValue = 0;
    private int _minInt = 0;
    private int _maxInt = 100;
    #endregion

    [Header("Color")]
    #region Color
    [SerializeField] private ColorPicker _colorPicker;
    #endregion

    [Header("Button")]
    [SerializeField] private Button _button;
    [Header("List of Buttons")]
    [SerializeField] private GameObject _buttonPrefab;
    [SerializeField] private Transform _parentButtons;
    private List<Button> _buttons = new List<Button>();

    public Button GetButton { get => _button; }
    public string SetID { set => _id = value; }
    public string GetID { get => _id; }
    private string _id { get; set; }

    private void Update()
    {
        UpdateBool();
        UpdateFloat();
        UpdateInt();
    }

    private void UpdateBool()
    {
        if (!_boolToggle) return;

        _boolReturnValue = _boolToggle.isOn;
    }

    private void UpdateFloat()
    {
        if (!_floatText) return;

        ConvertFromNormalizedFloat(_floatSlider.value);
    }

    private void UpdateInt()
    {
        if (!_intText) return;

        _intReturnValue = (int)_intSlider.value;
        _intReturnValue = Mathf.Clamp(_intReturnValue, _minInt, _maxInt);
        _floatText.text = _intReturnValue.ToString();
    }

    public void SetColor(Color c)
    {
        _colorReturnValue = c;
    }

    public void SetColorPicker(Color c)
    {
        _colorPicker.SetActualColor(c);
    }

    public void SetMinMaxFloat(float min, float max)
    {
        _min = min;
        _max = max;
        _minText.text = _min.ToString();
        _maxText.text = _max.ToString();
    }

    public void SetMinMaxInt(int min, int max)
    {
        _minInt = min;
        _maxInt = max;
        _minIntText.text = _minInt.ToString();
        _maxIntText.text = _maxInt.ToString();
        _intSlider.minValue = min;
        _intSlider.maxValue = max;
    }

    public dynamic getValue<T>()
    {
        if (typeof(T) == typeof(bool))
        {
            return _boolReturnValue;
        }
        else if (typeof(T) == typeof(float))
        {
            return _floatReturnValue;
        }
        else if (typeof(T) == typeof(int))
        {
            return _intReturnValue;
        }
        else if (typeof(T) == typeof(Color))
        {
            return _colorReturnValue;
        }
        else
        {
            Debug.LogError("Wrong type");
            return "Error";
        }
    }

    public float GetFloat()
    {
        return _floatReturnValue;
    }

    public float GetFloatNormalized()
    {
        return _floatSlider.value;
    }

    public void SetFloatNormalized(float f)
    {
        _floatSlider.value = f;
        ConvertFromNormalizedFloat(f);
    }

    public void ConvertFromNormalizedFloat(float normalizedFloat)
    {
        _floatReturnValue = Mathf.Lerp(_min, _max, normalizedFloat);
        _floatReturnValue = Mathf.Clamp(_floatReturnValue, _min, _max);
        _floatText.text = String.Format("{0:0.00}", _floatReturnValue);
    }

    public int GetInt()
    {
        return _intReturnValue;
    }

    public void SetInt(int number)
    {
        _intSlider.value = number;
        _intReturnValue = number;
    }

    public void SetBool(bool b)
    {
        _boolToggle.isOn = b;
        _boolReturnValue = b;
    }

    public bool GetBool()
    {
        return _boolReturnValue;
    }

    public Color GetColor()
    {
        return _colorReturnValue;
    }

    public void SetNameOfButton(string s)
    {
        _button.transform.Find("Text").GetComponent<Text>().text = s;
    }

    public void AddButtonToList(string nameForButton)
    {
        GameObject buttonGO = Instantiate(_buttonPrefab, _parentButtons);
        buttonGO.transform.localScale = Vector3.one;
        buttonGO.transform.SetSiblingIndex(0);
        Button button = buttonGO.GetComponent<Button>();
        _buttons.Add(button);

        //remove button
        Button removeButton = buttonGO.transform.Find("RemoveButton").GetComponent<Button>();
        removeButton.onClick.AddListener(() =>
        {
            RemoveButton(nameForButton);
            SaveFilesManager.Instance.DeleteFile(nameForButton);
        });

        //text
        Text text = buttonGO.transform.Find("Text").GetComponent<Text>();
        text.text = nameForButton;

        button.onClick.AddListener(() =>
        {
            SaveFilesManager.Instance.LoadFile(text.text);
        });

        GameObject go = new GameObject("Empty");
        go.transform.parent = transform.parent;
        go.transform.localScale = Vector3.one;
        RectTransform rt = go.AddComponent<RectTransform>();
        rt.sizeDelta = new Vector2(25f, 25f);
    }

    public void RemoveButton(string name)
    {
        Button b = _buttons.Find(x => x.transform.Find("Text").GetComponent<Text>().text == name);
        _buttons.Remove(b);

        Destroy(b.gameObject);

        Transform empty = transform.parent.Find("Empty");
        Destroy(empty.gameObject);
    }

    private void OnEnable()
    {
        if (!_boolToggle) return;

        _boolToggle.isOn = _boolReturnValue;
    }

    private void OnDisable()
    {
        if (!_boolToggle) return;

        _boolToggle.isOn = _boolReturnValue;
    }
}
