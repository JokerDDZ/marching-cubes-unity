using UnityEngine;
using UnityEngine.UI;

public class ColorPicker : MonoBehaviour
{
    [SerializeField] private GUIElementController _guiElementController;
    [SerializeField] private RectTransform _texture;
    [SerializeField] private Texture2D _sprite;
    [SerializeField] private Image _outImage;

    private bool _choosingColor = false;

    #region Events
    public void OnClickPicker()
    {
        _choosingColor = true;
        if (_choosingColor) SetColor();

    }

    public void OnDragPicker()
    {
        if (_choosingColor) SetColor();
    }

    public void DisablePickingColor()
    {
        _choosingColor = false;
    }
    #endregion

    private void SetColor()
    {
        Vector3 imagePos = _texture.position;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(_texture, Input.mousePosition, null, out Vector2 delta);
        float globalX = Input.mousePosition.x - imagePos.x;
        float globalY = Input.mousePosition.y - imagePos.y;
        int localPosX = (int)(delta.x * _sprite.width / _texture.rect.width);
        int localPosY = (int)(delta.y * _sprite.height / _texture.rect.height);
        Color c = _sprite.GetPixel(localPosX, localPosY);
        SetActualColor(c);
    }

    public void SetActualColor(Color c)
    {
        _outImage.color = c;
        _guiElementController.SetColor(c);
    }
}
