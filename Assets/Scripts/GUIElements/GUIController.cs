using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Tayx.Graphy;
using UnityEngine.UI;

public class GUIController : MonoBehaviour
{
    [SerializeField] private GameObject _buttonPrefab;
    [SerializeField] private Text _numberOfPoints;
    [SerializeField] private Text _numberOfUpdated;
    [SerializeField] private Transform _buttonsParent;
    [SerializeField] private GameObject _guiPanel;
    [SerializeField] private GraphyManager _graph;
    [SerializeField] private ScrollRect _scrollRect;
    [SerializeField] private List<RectTransform> _categories = new List<RectTransform>();
    [SerializeField] private List<PresetsByIndex> _menuPresets = new List<PresetsByIndex>();
    private List<GUIElementController> _elementControllers = new List<GUIElementController>();
    public string SetNumberOfPoints{set => _numberOfPoints.text = value;}
    public string SetNumberOfUpdated{set => _numberOfUpdated.text = value;}


    private void Awake()
    {
        foreach (PresetsByIndex m in _menuPresets)
        {
            InitMenuPreset(m);
        }

        foreach (RectTransform rt in _categories)
        {
            GameObject go = Instantiate(_buttonPrefab);
            go.transform.GetChild(0).GetComponent<Text>().text = rt.name;
            go.transform.SetParent(_buttonsParent, true);
            go.transform.localScale = Vector3.one;
        }

        StartCoroutine(DelayPanelOff());
    }

    private void Start()
    {
        _scrollRect.content = _categories[0];

        if (!_graph) _graph = FindObjectOfType<GraphyManager>(true);
    }

    private IEnumerator DelayPanelOff()
    {
        for (int i = 0; i < 2; i++)
        {
            yield return 0;
        }

        _guiPanel.SetActive(false);
        _buttonsParent.gameObject.SetActive(false);

        foreach (RectTransform rt in _categories)
        {
            rt.gameObject.SetActive(false);
        }

        _categories[0].gameObject.SetActive(true);
        EnableAllButtons();
        _buttonsParent.GetChild(0).GetComponent<Button>().interactable = false;

        for (int i = 0; i < _buttonsParent.childCount; i++)
        {
            Button button = _buttonsParent.GetChild(i).GetComponent<Button>();
            RectTransform category = _categories[i];

            button.onClick.AddListener(() =>
            {
                DisableAllCategories();
                EnableAllButtons();
                button.interactable = false;

                category.gameObject.SetActive(true);
                _scrollRect.content = category;
            });
        }

        if (_graph)
        {
            _graph.RamModuleState = GraphyManager.ModuleState.OFF;
            _graph.AudioModuleState = GraphyManager.ModuleState.OFF;
            _graph.AdvancedModuleState = GraphyManager.ModuleState.OFF;
        }
    }

    public dynamic ReturnValueFrom<T>(string id)
    {
        GUIElementController element = _elementControllers.Find(x => x.GetID == id);

        if (element == null)
        {
            Debug.LogError("Cant find element controller with id: " + id);
            return null;
        }
        else
        {
            var result = element.getValue<T>();
            return result;
        }
    }

    public dynamic ReturnValueFrom<T>(GUIElementController ele)
    {
        return ele.getValue<T>();
    }

    public GUIElementController GetElementController(string id)
    {
        GUIElementController element = _elementControllers.Find(x => x.GetID == id);

        if (element == null)
        {
            Debug.LogError("Cant find element controller with id: " + id);
            return null;
        }
        else
        {
            return element;
        }
    }

    public void ShowOnlyFPS(bool b)
    {
        _graph.FpsModuleState = GraphyManager.ModuleState.FULL;

        if (b)
        {
            _graph.RamModuleState = GraphyManager.ModuleState.OFF;
            _graph.AudioModuleState = GraphyManager.ModuleState.OFF;
            _graph.AdvancedModuleState = GraphyManager.ModuleState.OFF;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            HideGUI(false);
        }
        else
        {
            _graph.RamModuleState = GraphyManager.ModuleState.FULL;
            _graph.AudioModuleState = GraphyManager.ModuleState.FULL;
            _graph.AdvancedModuleState = GraphyManager.ModuleState.FULL;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            HideGUI(true);
        }
    }

    private void HideGUI(bool b)
    {
        _guiPanel.SetActive(b);
        _buttonsParent.gameObject.SetActive(b);
        _numberOfPoints.gameObject.SetActive(b);
    }

    private void EnableAllButtons()
    {
        foreach (Transform child in _buttonsParent)
        {
            child.GetComponent<Button>().interactable = true;
        }
    }

    private void DisableAllCategories()
    {
        foreach (Transform cate in _categories)
        {
            cate.gameObject.SetActive(false);
        }
    }

    private void InitMenuPreset(PresetsByIndex m)
    {
        GameObject goHandler = Instantiate(m.Preset.GetPrefab, m.Category);
        GUIElementController contr = goHandler.GetComponent<GUIElementController>();
        contr.SetHeader = m.Header;
        contr.SetID = m.ID;
        _elementControllers.Add(contr);
    }

    [Serializable]
    public struct PresetsByIndex
    {
        public MenuPresetSO Preset;
        public string ID;
        public string Header;
        public Transform Category;
    }
}
