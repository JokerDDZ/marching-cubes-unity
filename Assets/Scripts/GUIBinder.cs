using UnityEngine;
using System.Collections.Generic;
using System.IO;

public class GUIBinder : MonoBehaviour
{
    [SerializeField] private NoiseDensity _noiseDensity;
    [SerializeField] private MeshGenerator _meshGenerator;
    [SerializeField] private GUIController _guiController;
    [SerializeField] private MeshEditor _meshEditor;
    [SerializeField] private ColorGenerator _colorGenerator;
    [SerializeField] private Timelapse _timelapse;
    [SerializeField] private SaveFilesManager _saveFileManager;

    private GUIElementController _boundSize;
    private GUIElementController _pointsPerTile;
    private GUIElementController _isoLevel;
    private GUIElementController _seed;
    private GUIElementController _noiseScale;
    private GUIElementController _autoUpdate;
    private GUIElementController _radius;
    private GUIElementController _speed;
    private GUIElementController _debugAvaible;
    private GUIElementController _noiseDebugSize;
    private GUIElementController _guiTilesX;
    private GUIElementController _guiTilesY;
    private GUIElementController _guiTilesZ;
    private GUIElementController _guiColor1;
    private GUIElementController _guiColor2;
    private GUIElementController _guiColor3;
    private GUIElementController _guiColor4;
    private GUIElementController _guiColor5;
    private GUIElementController _guiDrawGizmos;
    private GUIElementController _showCenter;
    private GUIElementController _isTimelapse;
    private GUIElementController _timelapseSpeed;
    private GUIElementController _colorTimelapse;
    private GUIElementController _saveButton;
    private GUIElementController _listSaves;
    private List<ParticleDebug> _particleDebugs = new List<ParticleDebug>();

    //String optimization
    private const string _nameSeed = "Seed";
    private const string _nameIso = "IsoLevel";
    private const string _nameBoundSize = "BoundSize";
    private const string _namePoints = "NumPoints";
    private const string _nameScale = "NoiseScale";
    private const string _nameRadius = "Radius";
    private const string _nameTerrainSpeed = "ChangeTerrainSpeed";
    private const string _nameAutoUpdate = "AutoUpdate";
    private const string _nameNoiseDebug = "NoiseDebug";
    private const string _nameTileX = "TilesX";
    private const string _nameTileY = "TilesY";
    private const string _nameTileZ = "TilesZ";

    private void Start()
    {
        _guiController.GetElementController(_nameSeed).SetMinMaxInt(0, 2000);
        _guiController.GetElementController(_nameIso).SetMinMaxFloat(-10f, 10f);
        _guiController.GetElementController(_nameBoundSize).SetMinMaxFloat(1f, 70f);
        _guiController.GetElementController(_namePoints).SetMinMaxInt(2, 100);
        _guiController.GetElementController(_nameScale).SetMinMaxFloat(0f, 5f);
        _guiController.GetElementController(_nameRadius).SetMinMaxFloat(0f, 30f);
        _guiController.GetElementController(_nameTerrainSpeed).SetMinMaxFloat(1f, 20f);

        _boundSize = _guiController.GetElementController(_nameBoundSize);
        _pointsPerTile = _guiController.GetElementController(_namePoints);
        _isoLevel = _guiController.GetElementController(_nameIso);
        _seed = _guiController.GetElementController(_nameSeed);
        _noiseScale = _guiController.GetElementController(_nameScale);
        _autoUpdate = _guiController.GetElementController(_nameAutoUpdate);
        _radius = _guiController.GetElementController(_nameRadius);
        _speed = _guiController.GetElementController(_nameTerrainSpeed);
        _debugAvaible = _guiController.GetElementController(_nameNoiseDebug);
        _guiTilesX = _guiController.GetElementController(_nameTileX);
        _guiTilesY = _guiController.GetElementController(_nameTileY);
        _guiTilesZ = _guiController.GetElementController(_nameTileZ);
        _guiColor1 = _guiController.GetElementController("Color1");
        _guiColor2 = _guiController.GetElementController("Color2");
        _guiColor3 = _guiController.GetElementController("Color3");
        _guiColor4 = _guiController.GetElementController("Color4");
        _guiColor5 = _guiController.GetElementController("Color5");
        _guiDrawGizmos = _guiController.GetElementController("DrawGizmos");
        _noiseDebugSize = _guiController.GetElementController("SizeOfNoiseDebug");
        _showCenter = _guiController.GetElementController("ShowCenter");
        _isTimelapse = _guiController.GetElementController("Timelapse");
        _timelapseSpeed = _guiController.GetElementController("TimelapseSpeed");
        _colorTimelapse = _guiController.GetElementController("LightColor");
        _saveButton = _guiController.GetElementController("SaveButton");
        _listSaves = _guiController.GetElementController("ListSave");

        _noiseDebugSize.SetMinMaxFloat(.1f, 20f);

        _timelapseSpeed.SetMinMaxFloat(1f, 200f);
        _guiTilesX.SetMinMaxInt(1, 10);
        _guiTilesY.SetMinMaxInt(1, 10);
        _guiTilesZ.SetMinMaxInt(1, 10);

        RefreshSaveFileList();

        _saveButton.SetNameOfButton("Save");
        _saveButton.GetButton.onClick.AddListener(() =>
        {
            string nameOfFile = _saveFileManager.SaveFile();
            _listSaves.AddButtonToList(nameOfFile);
        });
    }

    private void Update()
    {
        _meshGenerator.SetShowGizmos = _guiDrawGizmos.GetBool();
        _meshGenerator.SetBoundSize = _boundSize.GetFloat();
        _meshGenerator.SetPointPerTile = _pointsPerTile.GetInt();
        _meshGenerator.SetIsoLevel = _isoLevel.GetFloat();
        _meshGenerator.SetNumberOfTiles = new Vector3Int(_guiTilesX.GetInt(), _guiTilesY.GetInt(), _guiTilesZ.GetInt());
        _noiseDensity.SetSeed = _seed.GetInt();
        _noiseDensity.SetNoiseScale = _noiseScale.GetFloat();
        _meshGenerator.SetAutoUpdate = _autoUpdate.GetBool();
        _meshEditor.SetRadius = _radius.GetFloat();
        _meshEditor.SetSpeed = _seed.GetFloat();
        _meshEditor.SetShowCenter = _showCenter.GetBool();
        _timelapse.SetTimeLapse = _isTimelapse.GetBool();
        _timelapse.SetTimeLapseSpeed = _timelapseSpeed.GetFloat();
        _timelapse.SetLightColor = _colorTimelapse.GetColor();

        _colorGenerator.GetColors[0] = _guiColor1.GetColor();
        _colorGenerator.GetColors[1] = _guiColor2.GetColor();
        _colorGenerator.GetColors[2] = _guiColor3.GetColor();
        _colorGenerator.GetColors[3] = _guiColor4.GetColor();
        _colorGenerator.GetColors[4] = _guiColor5.GetColor();

        foreach (ParticleDebug pd in _particleDebugs)
        {
            pd.SetDebugEnabled = _debugAvaible.GetBool();
            pd.SetCurrSizeNoise = _noiseDebugSize.GetFloat();
        }
    }

    public void SetValuesFromLoad(SaveFilesManager.SaveData sd)
    {
        //number of tiles
        _guiTilesX.SetInt(sd.NumberOfTiles.x);
        _guiTilesY.SetInt(sd.NumberOfTiles.y);
        _guiTilesZ.SetInt(sd.NumberOfTiles.z);

        _boundSize.SetFloatNormalized(sd.BoundSize);
        _isoLevel.SetFloatNormalized(sd.IsoLevel);

        _pointsPerTile.SetInt(sd.NumPoints);

        _guiColor1.SetColorPicker(sd.TerrainColors[0]);
        _guiColor2.SetColorPicker(sd.TerrainColors[1]);
        _guiColor3.SetColorPicker(sd.TerrainColors[2]);
        _guiColor4.SetColorPicker(sd.TerrainColors[3]);
        _guiColor5.SetColorPicker(sd.TerrainColors[4]);

        _seed.SetInt(sd.Seed);
        _noiseScale.SetFloatNormalized(sd.NoiseScale);

        _noiseDebugSize.SetFloatNormalized(sd.SizeOfNoiseDebug);
        _showCenter.SetBool(sd.ShowCenter);
        _debugAvaible.SetBool(sd.NoiseDebug);
        _guiDrawGizmos.SetBool(sd.DrawGizmos);
        _radius.SetFloatNormalized(sd.Radius);
        _speed.SetFloatNormalized(sd.ChangeTerrainSpeed);
        _autoUpdate.SetBool(sd.AutoUpdate);
        _isTimelapse.SetBool(sd.Timelapse);
        _timelapseSpeed.SetFloatNormalized(sd.TimeLapseSpeed);
        _colorTimelapse.SetColorPicker(sd.LightColor);
    }

    public SaveFilesManager.SaveData GetValuesForSave()
    {
        SaveFilesManager.SaveData sd = new SaveFilesManager.SaveData(new Vector3Int(_guiTilesX.GetInt(),_guiTilesY.GetInt(),_guiTilesZ.GetInt()),_meshGenerator,_pointsPerTile.GetInt());

        sd.BoundSize = _boundSize.GetFloatNormalized();
        sd.IsoLevel = _isoLevel.GetFloatNormalized();

        sd.TerrainColors[0] = _guiColor1.GetColor();
        sd.TerrainColors[1] = _guiColor2.GetColor();
        sd.TerrainColors[2] = _guiColor3.GetColor();
        sd.TerrainColors[3] = _guiColor4.GetColor();
        sd.TerrainColors[4] = _guiColor5.GetColor();

        sd.Seed = _seed.GetInt();
        sd.NoiseScale = _noiseScale.GetFloatNormalized();

        sd.SizeOfNoiseDebug = _noiseDebugSize.GetFloatNormalized();
        sd.ShowCenter = _showCenter.GetBool();
        sd.NoiseDebug = _debugAvaible.GetBool();
        sd.DrawGizmos = _guiDrawGizmos.GetBool();
        sd.Radius = _radius.GetFloatNormalized();
        sd.ChangeTerrainSpeed = _speed.GetFloatNormalized();
        sd.AutoUpdate = _autoUpdate.GetBool();
        sd.Timelapse = _isTimelapse.GetBool();
        sd.TimeLapseSpeed = _timelapseSpeed.GetFloatNormalized();
        sd.LightColor = _colorTimelapse.GetColor();

        return sd;
    }

    public void RefreshSaveFileList()
    {
        string[] namesOfFiles = Directory.GetFiles(_saveFileManager.GetPath);
        //Debug.Log("Number of loaded files: " + namesOfFiles.Length);

        foreach (string singleName in namesOfFiles)
        {
            string nameFile = Path.GetFileName(singleName);
            _listSaves.AddButtonToList(nameFile);
        }
    }

    public void AddParticleDebugToList(ParticleDebug pd)
    {
        if (!_particleDebugs.Contains(pd))
        {
            _particleDebugs.Add(pd);
        }
    }

    public void RemoveParticleDebugFromList(ParticleDebug pd)
    {
        if (_particleDebugs.Contains(pd))
        {
            _particleDebugs.Remove(pd);
        }
    }
}
