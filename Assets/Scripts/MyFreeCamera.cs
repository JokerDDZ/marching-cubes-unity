using UnityEngine;
using Tayx.Graphy;

public class MyFreeCamera : MonoBehaviour
{
    [SerializeField] private GraphyManager _fpsDebug;
    [SerializeField] private GUIController _guiController;
    private const float _speed = 4f;
    private float _rotationX = 0f;
    private float _rotationY = 0f;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        GUIInput();

        if (_fpsDebug.AudioModuleState == GraphyManager.ModuleState.FULL) return;

        float currSpeed = _speed * (Input.GetKey(KeyCode.LeftShift) ? 3f : 1f);

        _rotationX += Input.GetAxis("Mouse X") * _speed;
        _rotationY += Input.GetAxis("Mouse Y") * _speed;
        transform.localRotation = Quaternion.AngleAxis(_rotationX, Vector3.up);
        transform.localRotation *= Quaternion.AngleAxis(_rotationY, Vector3.left);


        if (Input.GetKey(KeyCode.W))
        {
            transform.position += transform.forward * currSpeed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.position -= transform.forward * currSpeed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.A))
        {
            transform.position -= transform.right * currSpeed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.position += transform.right * currSpeed * Time.deltaTime;
        }
    }

    private void GUIInput()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            GraphyManager gm = _fpsDebug.GetComponent<GraphyManager>();
            bool hide = !(gm.AudioModuleState == GraphyManager.ModuleState.OFF);
            _guiController.ShowOnlyFPS(hide);
        }
    }
}
