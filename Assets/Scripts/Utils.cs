using UnityEngine;

public class Utils : MonoBehaviour
{
    //firstB is always 0 and secondB is always 1
    public static Vector2 FormulaLinearEquationForDebug(float firstA, float secondA)
    {
        float a = (1f) / (secondA - firstA);
        float b = -(a * firstA);

        return new Vector2(a, b);
    }

    public static void ChangeMeshScale(Mesh m, float scale, Vector3[] pattern)
    {
        Vector3[] baseVerts = m.vertices;

        for (int i = 0; i < baseVerts.Length; i++)
        {
            Vector3 handler = new Vector3(pattern[i].x * scale, pattern[i].y * scale, pattern[i].z * scale);
            baseVerts[i] = handler;
        }

        m.vertices = baseVerts;
        m.RecalculateNormals();
        m.RecalculateBounds();
    }

    public static bool DoesCubeIntersectSphere(Vector3 C1, Vector3 C2, Vector3 S, float R)
    {
        float dist_squared = R * R;
        if (S.x < C1.x) dist_squared -= Mathf.Pow((S.x - C1.x),2f);
        else if (S.x > C2.x) dist_squared -= Mathf.Pow((S.x - C2.x),2f);
        if (S.y < C1.y) dist_squared -= Mathf.Pow((S.y - C1.y),2f);
        else if (S.y > C2.y) dist_squared -= Mathf.Pow((S.y - C2.y),2f);
        if (S.z < C1.z) dist_squared -= Mathf.Pow((S.z - C1.z),2f);
        else if (S.z > C2.z) dist_squared -= Mathf.Pow((S.z - C2.z),2f);
        return dist_squared > 0;
    }
}
