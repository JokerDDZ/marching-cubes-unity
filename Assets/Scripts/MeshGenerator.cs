using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Profiling;

public class MeshGenerator : MonoBehaviour
{
    const int _threadSize = 8;
    [SerializeField] private GUIController _guiController;
    [SerializeField] private ParticleDebug _debugPrefab;
    [SerializeField] private GUIBinder _guiBinder;
    [SerializeField] private NoiseDensity _densityGen;
    [SerializeField] private Transform _tilesParent;
    [SerializeField] private ComputeShader _computeShaderMC;
    [SerializeField] private ComputeShader _resetForTiles;

    public Vector3Int GetNumberOfTiles { get => _numberOfTiles; }
    public Vector3Int SetNumberOfTiles { set => _numberOfTiles = value; }
    [SerializeField] private Vector3Int _numberOfTiles = Vector3Int.one;

    public List<Tile> Tiles { get => _tiles; }
    private List<Tile> _tiles = new List<Tile>();
    public Tile[,,] GetTilesAsMatrix { get => _tilesAsMatrix; }
    private Tile[,,] _tilesAsMatrix;

    #region Buffers
    private ComputeBuffer _trisBuffer;
    private ComputeBuffer _trisCountBuffer;
    #endregion
    private ColorGenerator _colorGenerator;
    public float SetIsoLevel { set => _isoLevel = value; }
    [SerializeField] private float _isoLevel = 2f;

    [Range(2, 100)]
    [SerializeField] private int _pointsPerTile = 30;
    public int SetPointPerTile { set => _pointsPerTile = value; }
    public int GetPointPerTile { get => _pointsPerTile; }

    public float SetBoundSize { set => _boundsSize = value; }
    public float GetBoundSize { get => _boundsSize; }
    [SerializeField] private float _boundsSize = 1;
    public bool SetAutoUpdate { set => _autoUpdate = value; }
    private bool _autoUpdate = false;
    public bool SetShowGizmos { set => _showGizmos = value; }
    private bool _showGizmos = true;

    private void Awake()
    {
        _colorGenerator = GetComponent<ColorGenerator>();
    }

    private void Start()
    {
        InitializeTiles();
        CreateBuffers();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || _autoUpdate)
        {
            RefreshTerrain();
        }
    }

    public void RefreshTerrain(bool generateNewValues = true)
    {
        CheckTiles();
        UpdateAllMeshes(generateNewValues);
        _guiController.SetNumberOfPoints = "Number of points: " + (_tiles.Count * _pointsPerTile * _pointsPerTile * _pointsPerTile).ToString();
    }

    private void OnDestroy()
    {
        ReleaseBuffers();
    }

    #region Buffers

    private void CreateBuffers()
    {
        int points = _pointsPerTile * _pointsPerTile * _pointsPerTile;
        int voxelPerAxis = _pointsPerTile - 1;
        int numVoxels = voxelPerAxis * voxelPerAxis * voxelPerAxis;
        int maxTriangleCount = numVoxels * 5;

        ReleaseBuffers();

        _trisBuffer = new ComputeBuffer(maxTriangleCount, sizeof(float) * 3 * 3, ComputeBufferType.Append);

        foreach (Tile t in _tiles)
        {
            t.SetVertexBufferWithArray(null, points);
        }

        _trisCountBuffer = new ComputeBuffer(1, sizeof(int), ComputeBufferType.Raw);
    }

    private void ReleaseBuffers()
    {
        if (_trisBuffer == null) return;

        _trisBuffer.Release();

        foreach (Tile t in _tiles)
        {
            if (t.GetVertexBuffer == null) continue;
            t.GetVertexBuffer.Release();
        }

        _trisCountBuffer.Release();
    }
    #endregion

    private void InitializeTiles()
    {
        _tilesAsMatrix = new Tile[_numberOfTiles.x, _numberOfTiles.y, _numberOfTiles.z];

        for (int x = 0; x < _numberOfTiles.x; x++)
        {
            for (int y = 0; y < _numberOfTiles.y; y++)
            {
                for (int z = 0; z < _numberOfTiles.z; z++)
                {
                    Vector3Int coord = new Vector3Int(x, y, z);
                    bool alreadyExists = false;

                    for (int i = 0; i < _tiles.Count; i++)
                    {
                        if (_tiles[i].GetCoord == coord)
                        {
                            alreadyExists = true;
                            break;
                        }
                    }

                    if (!alreadyExists)
                    {
                        Tile t = InitTile(coord);
                        t.SetForReset = _resetForTiles;
                        _tiles.Add(t);
                        _tilesAsMatrix[x, y, z] = t;
                    }
                }
            }
        }

        for (int i = 0; i < _tiles.Count; i++)
        {
            Tile t = _tiles[i];
            Vector3Int coord = t.GetCoord;

            for (int y = coord.y - 1; y < 2 + coord.y; y++)
            {
                if (y < 0 || y >= _numberOfTiles.y) continue;

                for (int x = coord.x - 1; x < 2 + coord.x; x++)
                {
                    if (x < 0 || x >= _numberOfTiles.x) continue;

                    for (int z = coord.z - 1; z < 2 + coord.z; z++)
                    {
                        if (z < 0 || z >= _numberOfTiles.z) continue;

                        t.GetNeighbours.Add(_tilesAsMatrix[x, y, z]);
                    }
                }
            }
        }
    }

    private void CheckTiles()
    {
        if (_tiles.Count != (_numberOfTiles.x * _numberOfTiles.y * _numberOfTiles.z))
        {
            for (int i = _tilesParent.childCount - 1; i >= 0; i--)
            {
                Destroy(_tilesParent.GetChild(i).gameObject);
            }

            _tiles.Clear();
            InitializeTiles();
            CreateBuffers();
        }
    }

    private Tile InitTile(Vector3Int coord)
    {
        GameObject go = new GameObject("tile");
        go.transform.parent = _tilesParent;
        Tile t = go.AddComponent<Tile>();
        ParticleDebug pd = Instantiate(_debugPrefab, go.transform);
        _guiBinder.AddParticleDebugToList(pd);
        t.SetParticleDebug = pd;
        t.SetCoord = coord;
        return t;
    }

    public void UpdateAllMeshes(bool generateNewValues = true)
    {
        if (generateNewValues) CreateBuffers();

        for (int i = 0; i < _tiles.Count; i++)
        {
            UpdateTilesMesh(_tiles[i], generateNewValues);
        }
    }

    public void UpdateTilesMesh(Tile tile, bool generateNewValues = true)
    {
        int numVoxelsPerAxis = _pointsPerTile - 1;
        int numThreadsPerAxis = Mathf.CeilToInt(numVoxelsPerAxis / (float)_threadSize);
        float pointSpacing = _boundsSize / (_pointsPerTile - 1);

        Vector3Int coord = tile.GetCoord;
        Vector3 centre = CentreFromCoord(coord);
        Vector3 worldBounds = new Vector3(_numberOfTiles.x, _numberOfTiles.y, _numberOfTiles.z);

        if (generateNewValues)
        {
            _densityGen.Generate(tile.GetVertexBuffer, _pointsPerTile, _boundsSize, worldBounds, centre, Vector3.zero, pointSpacing);
        }

        _trisBuffer.SetCounterValue(0);
        _computeShaderMC.SetBuffer(0, "points", tile.GetVertexBuffer);
        _computeShaderMC.SetBuffer(0, "triangles", _trisBuffer);
        _computeShaderMC.SetInt("numPointsPerAxis", _pointsPerTile);
        _computeShaderMC.SetFloat("isoLevel", _isoLevel);
        _computeShaderMC.Dispatch(0, numThreadsPerAxis, numThreadsPerAxis, numThreadsPerAxis);
        tile.GetParticleDebug.RefreshDebug(tile.GetVertexBuffer);
        ComputeBuffer.CopyCount(_trisBuffer, _trisCountBuffer, 0);
        int[] triCountArray = { 0 };
        _trisCountBuffer.GetData(triCountArray);
        int numTris = triCountArray[0];

        Profiler.BeginSample("Get Data from buffer");
        // Get triangle data from shader
        Tri[] tris = new Tri[numTris];
        _trisBuffer.GetData(tris, 0, 0, numTris);
        Profiler.EndSample();

        Mesh mesh = tile.Mesh;
        mesh.Clear();

        var vertices = new Vector3[numTris * 3];
        var meshTriangles = new int[numTris * 3];

        for (int i = 0; i < numTris; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                meshTriangles[i * 3 + j] = i * 3 + j;
                vertices[i * 3 + j] = tris[i][j];
            }
        }

        mesh.vertices = vertices;
        mesh.triangles = meshTriangles;

        mesh.RecalculateNormals();
        _colorGenerator.SetupColorForMaterial();
        tile.SetUp();
    }

    public Vector3 CentreFromCoord(Vector3Int coord)
    {
        Vector3 totalBounds = (Vector3)_numberOfTiles * _boundsSize;
        return -totalBounds / 2 + (Vector3)coord * _boundsSize + Vector3.one * _boundsSize / 2;
    }

    private void OnDrawGizmos()
    {
        if (_showGizmos)
        {
            foreach (Tile t in _tiles)
            {
                if (t.GetCloseOne)
                {
                    Gizmos.color = Color.green;
                }
                else
                {
                    Gizmos.color = Color.white;
                }

                Gizmos.DrawWireCube(CentreFromCoord(t.GetCoord), Vector3.one * _boundsSize * .95f);
            }
        }
    }

    struct Tri
    {
        public Vector3 x;
        public Vector3 y;
        public Vector3 z;

        public Vector3 this[int i]
        {
            get
            {
                switch (i)
                {
                    case 0:
                        return x;
                    case 1:
                        return y;
                    default:
                        return z;
                }
            }
        }
    }
}
