using UnityEngine;

[CreateAssetMenu(fileName = "Menu Data", menuName = "ScriptableObjects/Menu Data", order = 1)]
public class MenuPresetSO : ScriptableObject
{
    public GameObject GetPrefab{get => _guiPrefab;}
    [SerializeField] private GameObject _guiPrefab;
}
