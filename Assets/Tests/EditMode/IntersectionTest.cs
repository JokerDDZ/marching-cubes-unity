using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class IntersectionTest
{
    [Test]
    public void Min_M2_M2_M2_Max_2_2_2_S_0_0_0_R_1()
    {
        Assert.AreEqual(true, Utils.DoesCubeIntersectSphere(new Vector3(-2f, -2f, -2f), new Vector3(2f, 2f, 2f), new Vector3(0f, 0f, 0f), 1f));
    }

    [Test]
    public void Min_M2_M2_M2_Max_2_2_2_S_10_10_10_R_1()
    {
        Assert.AreEqual(false, Utils.DoesCubeIntersectSphere(new Vector3(-2f, -2f, -2f), new Vector3(2f, 2f, 2f), new Vector3(10f, 10f, 10f), 1f));
    }

    [Test]
    public void Min_M2_M2_M2_Max_2_2_2_S_10_10_10_R_10()
    {
        Assert.AreEqual(false, Utils.DoesCubeIntersectSphere(new Vector3(-2f, -2f, -2f), new Vector3(2f, 2f, 2f), new Vector3(10f, 10f, 10f), 10f));
    }

    [Test]
    public void Min_M2_M2_M2_Max_2_2_2_S_10_10_10_R_3()
    {
        Assert.AreEqual(false, Utils.DoesCubeIntersectSphere(new Vector3(-2f, -2f, -2f), new Vector3(2f, 2f, 2f), new Vector3(10f, 10f, 10f), 3f));
    }

    [Test]
    public void Min_M2_M2_M2_Max_2_2_2_S_10_10_10_R_12()
    {
        Assert.AreEqual(false, Utils.DoesCubeIntersectSphere(new Vector3(-2f, -2f, -2f), new Vector3(2f, 2f, 2f), new Vector3(10f, 10f, 10f), 12f));
    }

    [Test]
    public void Min_M2_M2_M2_Max_2_2_2_S_10_10_10_R_14()
    {
        Assert.AreEqual(true, Utils.DoesCubeIntersectSphere(new Vector3(-2f, -2f, -2f), new Vector3(2f, 2f, 2f), new Vector3(10f, 10f, 10f), 14f));
    }
}
