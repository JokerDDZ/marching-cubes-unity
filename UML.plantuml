@startuml UML

class MeshGenerator{
    -GUIController _guiController
    -ParticleDebug _debugPrefab
    -GUIBinder _guiBinder
    -NoiseDensity _densityGen
    -Transform _tilesParent
    -ComputeShader _computeShaderMC
    -ComputeShader _resetForTiles
    -Vector3Int _numberOfTiles
    -List<Tile> _tile
    -Tile[,,] _tilesAsMatrix
    -ComputerBuffer _trisBuffer
    -ComputerBuffer _trisCountBuffer
    -ColorGenerator _colorGenerator
    -float _isoLevel
    -int _pointsPerTile
    -float _boundSize
    -bool _autoUpdate
    -bool _showGizmos
    +void RefreshTerrain(bool)
    -void CreateBuffers()
    -void ReleaseBuffers()
    -void InitializeTiles()
    -void CheckTiles()
    -Tile InitTile(Vector3Int)
    +void UpdateAllMeshes(bool)
    +void UpdateTilesMesh(Tile,bool)
    +Vector3 CentreFromCoord(Vector3Int)
}

class MeshEditor{
    -GameObject _centerGO
    -ComputeShader _editingCompute
    -GraphyManager _fpsCounter
    -Transform _camera
    -GUIController _guiController
    -Tile _closestOne
    -MeshGenerator _meshGenerator
    -int[] _modifRead
    -float _radius
    -float _speed
    -int _threadSize
    -bool _showCenter

}

class MyFreeCamera{
    -GraphyManager _fpsDebug
    -GUIController _guiController
    -const float _speed
    -float _rotationX
    -float _rotationY
    -void GUIInput()
}

class ColorGenerator{
    -Material _tilesAsMatrix
    -MeshGenerator _meshGenerator
    -Texture2D _texture
    -int _textureRes
    -Color[] _colors
    +void SetupColorForMaterial()
}

class NoiseDensity{
    -int _seed
    -int _numOctaves
    -float _lacunarity
    -float _persistence
    -float _noiseScale
    -float _noiseWeight
    -bool _closeEdges
    -float _floorOffset
    -float _weightMulti
    -float _hardFloorHeight
    -float _hardFloorWeight
    -Vector4 _shaderParams
}

class ParticleDebug{
    -Mesh _mesh
    +Material material
    -ComputeBuffer argsBuffer
    -Vector3[] _vertsPattern
    -uint[] args
    -Bounds bounds
    -bool _initialized
    -bool _debugEnabled
    -float _currSizeNoise
    -float _oldSizeNoise
    +void RefreshDebug()
}

class SaveFilesManager{
    {static}+SaveFilesManager Instance
    -MeshGenerator _meshGenerator
    -GUIBinder _guiBinder
    -string _path
    +string SaveFile()
    -void LoadFile(string)
    +void DeleteFile(string)
}

class Utils{
    {static}+Vector2 FormulaLinearEquationForDebug(float,float)
    {static}+void ChangeMeshScale(Mesh, float, Vector3[])
    {static}+bool DoesCubeIntersectSphere(Vector3, Vector3, Vector3, float)
}

class Timelapse{
    -Light _light
    -Color _lightColor
    -float _timeLapseSpeed
    -bool _timeLapse
}

class Tile{
    -ComputeShader _forReset
    -ParticleDebug _particleDebug
    +Mesh Mesh
    -MeshFilter _meshFilter
    -MeshRenderer _meshRenderer
    -MeshCollider _meshCollider
    -ComputeBuffer _vertexBuffer
    -List<Tile> _neighbours
    -Vector3Int _coord
    -bool _modified
    -bool _closeOne
    +void SetVertexBufferWithArray(float[], int)
    +void SetUp()
}

class ColorPicker{
    -GUIElementController _guiElementController
    -RectTransform _texture
    -Texture2D _sprite
    -Image _outImage
    +void OnClickPicker()
    +void OnDragPicker()
    +void DisablePickingColor()
    +void SetActualColor(Color c)
}

class DensityGenerator{
    #ComputeShader _densityComputeShader
    #List<ComputeBuffer> _bufferRel
    +{virtual} ComputeBuffer Generate(ComputeBuffer, int, float, Vector3 , Vector3 , Vector3 , float)
}

class GUIElementController{
    -Text _header
    -Toggle _boolToggle
    -Color _colorReturnValue
    -bool _boolReturnValue
    -Slider _floatSlider
    -Text _floatText
    -Text _minText
    -Text _maxText
    -float _floatReturnValue
    -float _min
    -float _max
    -Slider _intSlider
    -Text _intText
    -Text _minIntText
    -Text _maxIntText
    -int _intReturnValue
    -int _minInt
    -int _maxInt
    -ColorPicker _colorPicker
    -Button _button
    -GameObject _buttonPrefab
    -Transform _parentButtons
    -List<Button> _buttons
    -string _id
    -void UpdateBool()
    -void UpdateFloat()
    -void UpdateInt()
    +void ConvertFromNormalizedFloat(float)
    +void AddButtonToList(string)
    +void RemoveButton(string)
}

class GUIController{
    -GameObject _buttonPrefab
    -Text _numberOfPoints
    -Text _numberOfUpdated
    -Transform _buttonsParent
    -GameObject _guiPanel
    -GraphyManager _graph
    -ScrollRect _scrollRect
    -List<RectTransform> _categories
    -List<PresetsByIndex> _menuPresets
    -List<GUIElementController> _elementControllers 
    +void ShowOnlyFPS(bool)
    -void HideGUI(bool)
    -void EnableAllButtons()
    -void DisableAllCategories()
    -void InitMenuPreset(PresetsByIndex)
}

class GUIBinder{
    -NoiseDensity _noiseDensity
    -MeshGenerator _meshGenerator
    -GUIController _guiController
    -MeshEditor _meshEditor
    -ColorGenerator _colorGenerator
    -Timelapse _timelapse
    -SaveFilesManager _saveFileManager
    GUIElementController _guiElementControllers

    -List<ParticleDebug> _particleDebugs
    +SaveFilesManager.SaveData GetValuesForSave()
    +void RefreshSaveFilesList()
    +void AddParticleDebugToList(ParticleDebug)
    +void RemoveParticleDebugFromList(ParticleDebug)
}

class GraphyManager{

}

NoiseDensity --|> DensityGenerator
NoiseDensity "1" --o "1" MeshGenerator

Timelapse "1" --o "1" GUIBinder

Utils "1" --o "1..*" ParticleDebug
Utils "1" --o "1" MeshEditor

Tile "1..*" --o "1" MeshGenerator 

SaveFilesManager "1" --* "1" GUIBinder
SaveFilesManager "1" --o "1..*" GUIElementController
SaveFilesManager "1" --* "1" MeshGenerator

ParticleDebug "1..*" --o "1" MeshGenerator

MyFreeCamera "1" --* "1" GUIController
MyFreeCamera "1" --* "1" GraphyManager

MeshEditor "1" --* "1" MeshGenerator

GUIBinder "1" --* "1" MeshGenerator
GUIBinder "1" --o "1" ColorGenerator

ColorGenerator "1" --* "1" MeshGenerator

GUIController "1" --* "1" GUIBinder

ColorPicker "1" --* "1" GUIElementController

GUIElementController "1" --* "1" GUIController
@enduml